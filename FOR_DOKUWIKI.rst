How to use mathjax-siunitx with dokuwiki (as a debian package)
==============================================================

Prerequisites
-------------

 * install dokuwiki package
 * install mathjax-siunitx package
 * check that the package libjs-mathjax is installed

Special configuration
---------------------

 * Check that the permissions of the directory :code:`/etc/dokuwiki` are
   :code:`775`
 * ensure that the group www-data can write to file `/etc/dokuwiki/local.php`

Configuring Dokuwiki
--------------------

This must be done by `admin` user in the Dokuwiki web site.

Go to the administration page, and select **Extension manager**; install the
extension **Mathjax**

Go to the administration page, and select **Configuration Settings**; when
the page is loaded, you can click on the link *Mathjax* in the table of
contents.

In the *Mathjax section**, ensure the following settings:

The URL from which MathJax will be loaded
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:code:`/javascript/mathjax/MathJax.js?config=TeX-AMS_CHTML.js`

MathJax configuration; javascript code executed when MathJax loads
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This part is *not* optional, if one wants to configure siunitx properly.

.. code-block:: javascript
	  
      MathJax.Hub.Config({
        tex2jax: {inlineMath: [["$","$"],["\\(","\\)"]]},
        TeX: { extensions: [
          "[siunitx]/unpacked/siunitx.js",
          "color.js"
        ] }
      });
      MathJax.Ajax.config.path['siunitx']  = '/javascript/siunitx';

Screenshot
----------

.. image:: mathjax-screenshot1.png

	   
